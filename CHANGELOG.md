# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.0 - 2024-10-02(07:41:01 +0000)

### Other

- Implement unprivileged containers
- Idmap layer wrongly removed on containers Uninstall

## Release v0.2.5 - 2024-07-15(12:40:34 +0000)

### Other

- LCM container signature verification

## Release v0.2.4 - 2024-07-12(13:54:03 +0000)

## Release v0.2.3 - 2024-05-06(13:55:30 +0000)

### Other

- add a filter on the RPC dump function

## Release v0.2.2 - 2024-05-03(16:00:53 +0000)

### Other

- Add container execute command capability

## Release v0.2.1 - 2024-03-18(10:39:56 +0000)

### Other

- Add missing dependency on libsahtrace

## Release v0.2.0 - 2024-03-14(08:21:56 +0000)

### Other

- Add LCM flexible assert/logging macros

## Release v0.1.6 - 2023-10-04(13:55:54 +0000)

### Other

- Opensource component
- - Update license

## Release v0.1.5 - 2023-09-07(10:18:01 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.1.4 - 2023-06-16(13:52:12 +0000)

## Release v0.1.3 - 2023-05-26(08:25:02 +0000)

## Release v0.1.2 - 2023-05-17(09:10:18 +0000)

## Release v0.1.1 - 2023-03-21(13:03:38 +0000)

### Other

- fix asserts

## Release v0.1.0 - 2023-03-20(11:00:11 +0000)

## Release v0.0.1 - 2023-03-06(10:11:46 +0000)

### Other

- add a generic worker thread

## Release 0.0.1 - 2023-03-01(08:17:19 +0000)

### New

- initial release
