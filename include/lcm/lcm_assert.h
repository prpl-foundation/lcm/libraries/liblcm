/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__LCM_ASSERT_H__)
#define __LCM_ASSERT_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * The following macros are depracted.
 * Kept for backward compatibility
 */
#include <amxc/amxc_macros.h>
#define string_is_empty(s) (!s || !*s)

#define when_failed_log(x, l) if((x) != 0) { SAH_TRACEZ_ERROR(ME, "ASSERT " #x " FAILED"); goto l; }
#define when_null_log(x, l) if((x) == NULL) { SAH_TRACEZ_ERROR(ME, "ASSERT " #x " IS NULL"); goto l; }
#define when_not_null_log(x, l) if((x) != NULL) { SAH_TRACEZ_ERROR(ME, "ASSERT " #x " IS NOT NULL"); goto l; }
#define when_true_log(x, l) if(x) {  SAH_TRACEZ_ERROR(ME, "ASSERT " #x " IS TRUE"); goto l; }
#define when_false_log(x, l) if(!(x)) {  SAH_TRACEZ_ERROR(ME, "ASSERT " #x " IS FALSE"); goto l; }
#define when_str_empty_log(x, l) if(string_is_empty(x)) {SAH_TRACEZ_ERROR(ME, "ASSERT STRING " #x " IS EMPTY"); goto l; }
/**
 * End depracted macros
 */

/**
 *   LCM assertion.
 *   This file defines flexible assertion macros for capturing errors and eventually logging a message before executing an instruction.
 *       ASSERT_XXX_LOG captures unexpected situations with logging error, executing the requested instruction if 'x' evaluation is false by XXX.
 *       ASSERT_XXX captures unexpected situations without logging but only executing the requested instruction if 'x' evaluation is false by XXX.
 *       XXX could be one of the defined operations: NOT_NULL, TRUE, FALSE, SUCCESS, STR_NOT_EMPTY, NOT_EQUAL, EQUAL
 *
 *   The ASSERT_XXX_LOG macro can handle a custom error message with variables (va_list) of up to 10 variables. If no custom message is provided, a default one is used.
 *   Parameters:
 *       x: The variable to be evaluated.
 *       instruction: The instruction to be executed if the condition is true. NO_INSTRUCTION to be used when no instruction should follow the check.
 *       errMsg: Optional parameter for ASSERT_XXX_LOG. If not provided, a default text message will be used.
 *   Examples:
 *       ASSERT_TRUE(x, return 0);
 *       if(!(x)) {
 *           SAH_TRACEZ_ERROR(ME, "x is False");
 *           return 0;
 *       }
 *
 *       ASSERT_FALSE_NL(x, return);
 *       if((x)) {
 *           return ;
 *       }
 *
 *       ASSERT_STR_NOT_EMPTY(s, return 0);
 *       if((!s || !*s)) {
 *           SAH_TRACEZ_ERROR(ME, "s is Empty");
 *           return 0;
 *      }
 *
 *       ASSERT_SUCCESS(x, goto label, "Variable x is %d", x);
 *       if((x) != 0) {
 *           SAH_TRACEZ_ERROR(ME, "Variable x is %d", x);
 *           goto label;
 *       }
 *
 *       ASSERT_NOT_NULL(p, goto exit);
 *       if((p) == NULL) {
 *           SAH_TRACEZ_ERROR(ME, "p is Null");
 *           goto exit;
 *       }
 *
 *       ASSERT_TRUE(strcmp(s, "test"), goto exit, "Variable s (%s) is not 'test'", s);
 *       if(!(strcmp(s, "test"))) {
 *           SAH_TRACEZ_ERROR(ME, "Variable s (%s) is not 'test'", s);
 *           goto exit;
 *       }
 *
 *       ASSERT_NOT_EQUAL_NL(x, y, return 0);
 *       if((x) == (y)) {
 *           return 0;
 *       }
 *
 *       ASSERT_NOT_NULL_NL(s, goto exit);
 *       if((s) == NULL) {
 *           goto exit;
 *       }
 */
#define ASSERT_NOT_NULL_NL(x, instruction) VERIFY_NO_LOG((x) == NULL, instruction)
#define ASSERT_TRUE_NL(x, instruction) VERIFY_NO_LOG(!(x), instruction)
#define ASSERT_FALSE_NL(x, instruction) VERIFY_NO_LOG((x), instruction)
#define ASSERT_SUCCESS_NL(x, instruction) VERIFY_NO_LOG((x) != 0, instruction)
#define ASSERT_STR_NOT_EMPTY_NL(x, instruction) VERIFY_NO_LOG(STRING_EMPTY(x), instruction)
#define ASSERT_NOT_EQUAL_NL(x, y, instruction) VERIFY_NO_LOG((x) == (y), instruction)
#define ASSERT_EQUAL_NL(x, y, instruction) VERIFY_NO_LOG((x) != (y), instruction)
#define ASSERT_STR_EQUAL_NL(x, y, instruction) VERIFY_NO_LOG((strcmp(x, y) != 0), instruction)
#define ASSERT_STR_NOT_EQUAL_NL(x, y, instruction) VERIFY_NO_LOG((strcmp(x, y) == 0), instruction)

#define ASSERT_NOT_NULL(x, instruction, ...) VERIFY_AND_LOG(ERROR, x, is Null, (x) == NULL, instruction, ## __VA_ARGS__)
#define ASSERT_TRUE(x, instruction, ...) VERIFY_AND_LOG(ERROR, x, is False, !(x), instruction, ## __VA_ARGS__)
#define ASSERT_FALSE(x, instruction, ...) VERIFY_AND_LOG(ERROR, x, is True, (x), instruction, ## __VA_ARGS__)
#define ASSERT_SUCCESS(x, instruction, ...) VERIFY_AND_LOG(ERROR, x, is Failure, (x) != 0, instruction, ## __VA_ARGS__)
#define ASSERT_STR_NOT_EMPTY(x, instruction, ...) VERIFY_AND_LOG(ERROR, x, is Empty, STRING_EMPTY(x), instruction, ## __VA_ARGS__)
#define ASSERT_NOT_EQUAL(x, y, instruction, ...) VERIFY_AND_LOG(ERROR, (x and y), are Equal, (x) == (y), instruction, ## __VA_ARGS__)
#define ASSERT_EQUAL(x, y, instruction, ...) VERIFY_AND_LOG(ERROR, (x and y), are Different, (x) != (y), instruction, ## __VA_ARGS__)
#define ASSERT_STR_EQUAL(x, y, instruction, ...) VERIFY_AND_LOG(ERROR, (x and y), are Different, (strcmp(x, y) != 0), instruction, ## __VA_ARGS__)
#define ASSERT_STR_NOT_EQUAL(x, y, instruction, ...) VERIFY_AND_LOG(ERROR, (x and y), are Equal, (strcmp(x, y) == 0), instruction, ## __VA_ARGS__)

/*   LCM Logging.
 *   A flexible logging macro is defined for simple checking, logging and instruction execution.
 *   This macros cannot be changed without a risk to break the code behavior as these checks are part of the code unlike asserts.
 *       CHECK_XXX_LOG (level, x, instruction, errMsg, ...)
 *   Parameters:
 *       level: One of the SAH_TRACEZ levels (INFO, NOTICE, WARNING, ERROR)
 *       x: The variable to be evaluated by the XXX operation (NULL, NOT_NULL, TRUE, FALSE, FAILURE, SUCCESS, STR_EMPTY, EQUAL, NOT_EQUAL)
 *       instruction: The instruction to be executed if the condition is true. NO_INSTRUCTION to be used when no instruction should follow the check.
 *       errMsg: Optional parameter for CHECK_XXX_LOG. If not provided, a default text message will be used.
 *
 *   The logging macros can also receive custom log messages with variables (va_list) or up to 10 variables. If no custom message is provided, a default one will be used.
 *   Examples:
 *       CHECK_FALSE_LOG(INFO, x, goto end, "Nothing to do as x (%d) is False", x);
 *       if(!(x)) {
 *           SAH_TRACEZ_INFO(ME, "Nothing to do as x (%d) and y (%d) are different", x, y);
 *           goto end;
 *       }
 *
 *       CHECK_NOT_EQUAL_LOG(WARNING, x, y, goto end);
 *       if((x) != (y)) {
 *           SAH_TRACEZ_WARNING(ME, "(x and y) are not Equal");
 *           goto end;
 *       }
 */
#define CHECK_NULL_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is Null, (x) == NULL, instruction, ## __VA_ARGS__)
#define CHECK_NOT_NULL_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is not Null, (x) != NULL, instruction, ## __VA_ARGS__)
#define CHECK_TRUE_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is True, (x), instruction, ## __VA_ARGS__)
#define CHECK_FALSE_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is False, !(x), instruction, ## __VA_ARGS__)
#define CHECK_FAILURE_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is Failure, (x) != 0, instruction, ## __VA_ARGS__)
#define CHECK_SUCCESS_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is Suceess, (x) == 0, instruction, ## __VA_ARGS__)
#define CHECK_STR_EMPTY_LOG(level, x, instruction, ...) VERIFY_AND_LOG(level, x, is Empty, STRING_EMPTY(x), instruction, ## __VA_ARGS__)
#define CHECK_EQUAL_LOG(level, x, y, instruction, ...) VERIFY_AND_LOG(level, (x and y), are Equal, (x) == (y), instruction, ## __VA_ARGS__)
#define CHECK_NOT_EQUAL_LOG(level, x, y, instruction, ...) VERIFY_AND_LOG(level, (x and y), are Different, (x) != (y), instruction, ## __VA_ARGS__)
#define CHECK_STR_EQUAL_LOG(level, x, y, instruction, ...) VERIFY_AND_LOG(level, (x and y), are Equal, (strcmp(x, y) == 0), instruction, ## __VA_ARGS__)
#define CHECK_STR_NOT_EQUAL_LOG(level, x, y, instruction, ...) VERIFY_AND_LOG(level, (x and y), are Different, (strcmp(x, y) != 0), instruction, ## __VA_ARGS__)

/**
 *   Helper macros.
 *   Do not use any of the following macros directly. ASSERT_XXX, ASSERT_XXX_NL and CHECK_XXX_LOG should be used.
 *   Helpers used to select the correct logging format (level, error message, ...)
 *
 *   GET_MACRO_BY_ARGS helper macro handle up to 10 params (variadic).
 *   The number of parameters can be increased by increasig the GET_MACRO_BY_ARGS indexes as much as needed.
 */
#define NO_INSTRUCTION
#define STRING_EMPTY(s) (!s || !*s)

#define GET_MACRO_BY_ARGS(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, NAME, ...) NAME
#define VERIFY_AND_LOG(level, var, defaultlog, condition, instruction, ...) \
    GET_MACRO_BY_ARGS(, ## __VA_ARGS__, \
                      VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, \
                      VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, VERIFY_LOG_IMP, \
                      VERIFY_DEFAULT_LOG_IMP)(level, var, defaultlog, condition, instruction, __VA_ARGS__)

#define VERIFY_NO_LOG(condition, instruction) CHECK_IMP(condition, instruction)


#define DEFAULT_MSG(var, defaultlog) #var " " #defaultlog

#define VERIFY_DEFAULT_LOG_IMP(level, var, defaultlog, condition, instruction, ...) \
    VERIFY_LOG_IMP(level, var, defaultlog, condition, instruction, DEFAULT_MSG(var, defaultlog))

#define VERIFY_LOG_IMP(level, var, defaultlog, condition, instruction, errMsg, ...) \
    if(condition) { \
        SAH_TRACEZ_ ## level(ME, errMsg, ## __VA_ARGS__); \
        instruction; \
    }

#define CHECK_IMP(condition, instruction) \
    if(condition) { \
        instruction; \
    }

#ifdef __cplusplus
}
#endif

#endif // __LCM_ASSERT_H__
