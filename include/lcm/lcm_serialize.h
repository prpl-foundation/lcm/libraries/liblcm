/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__LCM_SERIALIZE_H__)
#define __LCM_SERIALIZE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef __AMXC_VARIANT_H__
#error "Missing include <amxc/amxc_variant.h>"
#endif
#ifndef errno
#error "Missing include <errno.h>"
#endif
#ifndef _STRING_H
#error "Missing include <string.h>"
#endif

/**
 * @brief macro to write data to a fd
 *
 * on failure, the macro will call fail_instruction
 *
 * @return the number of bytes send
 *
 */
#define checked_write(fd, data, fail_instruction) \
    ({ \
        int write_res; \
        do { \
            if((write_res = write(fd, &data, sizeof(data))) != sizeof(data)) { \
                if(write_res == 0) { \
                    SAH_TRACEZ_ERROR(ME, "Failed to send [" #data "] because fd is closed"); \
                    fail_instruction; \
                    break; \
                } \
                if(write_res < 0) { \
                    SAH_TRACEZ_ERROR(ME, "Failed to send [" #data "] (%d: %s)", errno, strerror(errno)); \
                    fail_instruction; \
                    break; \
                } \
            } \
        }while(0); \
        write_res; \
    })

/**
 * @brief macro to write data from a fd
 *
 * @return the number of bytes read
 *
 * on failure, the macro will call fail_instruction
 *
 */
#define checked_read(fd, data, fail_instruction) \
    ({ \
        int read_res; \
        do { \
            read_res = read(fd, &data, sizeof(data)); \
            if(read_res < 0 && errno != EWOULDBLOCK) { \
                SAH_TRACEZ_ERROR(ME, "Read error of [" #data "] (%d: %s)", errno, strerror(errno)); \
                fail_instruction; \
                break; \
            } \
            if(read_res == 0) { \
                SAH_TRACEZ_WARNING(ME, "socket closed unexpectedly"); \
                fail_instruction; \
                break; \
            } \
        } while (read_res < 1); \
        read_res; \
    })


int lcm_var_serialize(int fd, const amxc_var_t* var);
int lcm_var_deserialize(int fd, amxc_var_t* var);

#ifdef __cplusplus
}
#endif

#endif // __LCM_SERIALIZE_H__
