/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__LCM_WORKER_TASK_H__)
#define __LCM_WORKER_TASK_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>

#include <lcm/lcm_worker.h>

typedef int (* lcm_worker_task_fn_t) (lcm_worker_t* worker, amxc_var_t* var, int rc, amxc_var_t* data);

typedef struct _lcm_worker_task {
    lcm_worker_task_fn_t task_fn;
    amxc_var_t* task_var;
    int task_rc;    // this can contain the return code of the task, will be returned with the callback
    lcm_worker_task_fn_t callback_fn;
    amxc_var_t* callback_var;
    amxc_var_t* data; // this data will move between the worker process and back
    amxc_llist_it_t it;
} lcm_worker_task_t;

typedef enum _lcm_worker_sync_type {
    lcm_worker_sync_stop_worker = 0,
    lcm_worker_sync_add_task,
    lcm_worker_sync_add_task_front,
    lcm_worker_sync_none
} lcm_worker_sync_type_t;

typedef struct _lcm_worker_sync {
    lcm_worker_sync_type_t sync_type;
    lcm_worker_task_t task;
} lcm_worker_sync_t;

int lcm_worker_task_new(lcm_worker_task_t** task);
void lcm_worker_task_delete(lcm_worker_task_t** task);
void lcm_worker_task_it_free(amxc_llist_it_t* t_it);

/**
 * @brief Add a function that should be executed on a worker process
 *
 * @param task
 * @param fn The function
 * @param var Optional arguments for the task. This var is serialized, so it should not contain pointers.
 *            The caller of @ref lcm_worker_task_add_function should free the var after the call.
 *            A copy of the var is created on the task side. The function SHOULD NOT delete this var,
 *            it will be deleted by the task.
 * @return
 * 0 when the function and var are successfully added to the task, otherwise an error occurred
 */
int lcm_worker_task_add_function(lcm_worker_task_t* task,
                                 lcm_worker_task_fn_t fn,
                                 amxc_var_t* var);
/**
 * @brief add a callback function to the task
 *
 * this callback function will be called on the main process once the
 * function on the worker process is finished.
 *
 * @param task
 * @param fn the callback function
 * @param var Optional arguments for the task. The callback function SHOULD delete this var.
 * @return
 * 0 when the callback and var are successfully added to the task, otherwise an error occurred
 */
int lcm_worker_task_add_callback(lcm_worker_task_t* task,
                                 lcm_worker_task_fn_t fn,
                                 amxc_var_t* var);

/**
 * @brief add a callback function to the task
 *
 * this callback function will be called on the main process once the
 * function on the worker process is finished.
 *
 * @param task
 * @param data the callback function
 * @return
 * 0 when the callback and var are successfully added to the task, otherwise an error occurred
 */
int lcm_worker_task_add_data(lcm_worker_task_t* task, amxc_var_t* data);

/**
 * @brief add a worker task. The task will be deleted by the worker.
 * @param worker the worker process to which to add the task
 * @param task the task to add
 * @param add_to_front add the parameter to the front or to the back of the queue
 * @return
 * 0 when the task is successfully added to the worker, otherwise an error occurred
 */
int lcm_worker_add_task(lcm_worker_t* worker,
                        lcm_worker_task_t* task,
                        bool add_to_front);

#ifdef __cplusplus
}
#endif

#endif // __LCM_WORKER_TASK_H__
