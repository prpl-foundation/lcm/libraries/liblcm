/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#define ME "lcm_helpers"

#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

#include "lcm/lcm_helpers.h"
#include "lcm/lcm_assert.h"
#include <debug/sahtrace.h>


lcm_file_type_t lcm_filetype(const char* file) {
    struct stat fileStat;

    if(lstat(file, &fileStat) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not stat [%s] (%d: %s)", file, errno, strerror(errno));
        return FILE_TYPE_UNKNOWN;
    }

    if(S_ISREG(fileStat.st_mode)) {
        return FILE_TYPE_REGULAR;
    } else if(S_ISDIR(fileStat.st_mode)) {
        return FILE_TYPE_DIRECTORY;
    } else if(S_ISLNK(fileStat.st_mode)) {
        return FILE_TYPE_SYMLINK;
    } else if(S_ISCHR(fileStat.st_mode)) {
        return FILE_TYPE_CHAR_DEVICE;
    } else if(S_ISBLK(fileStat.st_mode)) {
        return FILE_TYPE_BLOCK_DEVICE;
    } else if(S_ISFIFO(fileStat.st_mode)) {
        return FILE_TYPE_PIPE;
    } else if(S_ISSOCK(fileStat.st_mode)) {
        return FILE_TYPE_SOCKET;
    } else {
        return FILE_TYPE_UNKNOWN;
    }
}

char* lcm_get_dirname(const char* path) {
    if(!path) {
        return NULL;
    }
    size_t end = strlen(path);
    while(end > 0 && path[end - 1] != '/') {
        end--;
    }
    if(end == 0) {
        return NULL;
    }
    char* res = calloc(end, sizeof(char));
    strncpy(res, path, end - 1);
    return res;
}

static bool dir_empty(const char* path) {
    bool res = false;
    DIR* dir = opendir(path);
    int n = 0;
    if(dir == NULL) {
        goto exit;
    }
    while((readdir(dir)) != NULL) {
        if(++n > 2) {
            goto exit;
        }
    }
    // dir is empty if only two entries are found, "." and ".."
    res = true;
exit:
    if(dir) {
        closedir(dir);
    }
    return res;
}

bool lcm_dir_empty(const char* path) {
    bool res = false;
    ASSERT_STR_NOT_EMPTY(path, return res, "Directory path is empty");
    ASSERT_EQUAL(lcm_filetype(path), FILE_TYPE_DIRECTORY, return res, "Directory %s is not a directory", path);

    return dir_empty(path);
}
