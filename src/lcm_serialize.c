/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#define ME "serialize"

#include <errno.h>
#include <string.h>
#include <yajl/yajl_gen.h>
#include <sys/socket.h>

#include <debug/sahtrace.h>
#include <amxc/amxc_variant_type.h>
#include <amxj/amxj_variant.h>
#include "lcm/lcm_assert.h"

#include "lcm/lcm_serialize.h"


/**
 * @brief
 *
 * @param fd
 * @param var
 * @return int -1 on error. Number of bytes serialized on success.
 */
int lcm_var_serialize(int fd, const amxc_var_t* var) {
    int res = -1;
    amxc_var_t var_json;
    const char* json_string = NULL;
    size_t len = 0;

    ASSERT_NOT_NULL(var, goto exit);

    amxc_var_init(&var_json);
    ASSERT_SUCCESS(amxc_var_convert(&var_json, var, AMXC_VAR_ID_JSON), goto exit, "Could not convert var to json");
    ASSERT_NOT_NULL(json_string = amxc_var_constcast(jstring_t, &var_json), goto exit, "Could not extract json string");
    len = strlen(json_string);
    ASSERT_EQUAL(res = write(fd, &len, sizeof(size_t)), sizeof(size_t), goto exit, "Write error (%d: %s)", errno, strerror(errno));
    ASSERT_EQUAL(res = write(fd, json_string, len), (int) len, goto exit, "Write error (%d: %s)", errno, strerror(errno));

exit:
    amxc_var_clean(&var_json);
    return res;

}

/**
 * @brief deserialize a variant from a fd
 *
 * @param fd
 * @param var
 * @return int -1 on error. 0 when fd is closed. Number of bytes deserialized on success.
 */
int lcm_var_deserialize(int fd, amxc_var_t* var) {
    int res = -1;
    int rcv_res = 0;
    amxc_var_t var_json;
    char* json_string = NULL;
    size_t len = 0;
    size_t total_read = 0;
    amxc_var_init(&var_json);

    ASSERT_NOT_NULL(var, goto exit);

    // wait for len data
    do {
        rcv_res = read(fd, &len, sizeof(size_t));
        if(rcv_res < 0) {
            if(errno != EWOULDBLOCK) {
                SAH_TRACEZ_ERROR(ME, "Read error (%d: %s)", errno, strerror(errno));
                goto exit;
            }
        }
        if(rcv_res == 0) {
            SAH_TRACEZ_WARNING(ME, "socket closed unexpectedly");
            res = 0;
            goto exit;
        }
    } while(rcv_res < 1);

    json_string = (char*) calloc(1, len + 1);
    // read the whole message
    while(total_read != len) {
        int read_len = read(fd, json_string + total_read, len - total_read);
        if(read_len < 0) {
            if(errno != EWOULDBLOCK) {
                SAH_TRACEZ_ERROR(ME, "Read error (%d: %s)", errno, strerror(errno));
                goto exit;
            }
        }
        if(read_len == 0) {
            SAH_TRACEZ_WARNING(ME, "socket closed unexpectedly");
            res = 0;
            goto exit;
        }
        total_read += read_len;
    }
    if(amxc_var_set(jstring_t, &var_json, json_string) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not transform string to json var [%s]", json_string);
        goto exit;
    }
    if(amxc_var_convert(var, &var_json, AMXC_VAR_ID_ANY) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not convert json to var");
        goto exit;
    }
    res = total_read;
exit:
    free(json_string);
    amxc_var_clean(&var_json);
    return res;
}


