/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>

#include <debug/sahtrace.h>
#include <amxc/amxc_llist.h>

#include "lcm/lcm_assert.h"
#include "lcm_priv.h"
#include "lcm/lcm_worker.h"
#include "lcm/lcm_worker_task.h"
#include "lcm/lcm_serialize.h"

#define ME "lcm_worker"

int lcm_worker_new(lcm_worker_t** worker, lcm_worker_init_fn_t init_fn, const char* name) {
    int res = -1;
    ASSERT_NOT_NULL(worker, goto exit);

    *worker = (lcm_worker_t*) calloc(1, sizeof(lcm_worker_t));
    ASSERT_NOT_NULL(*worker, goto exit);
    if(!STRING_EMPTY(name)) {
        strncpy((*worker)->name, name, sizeof((*worker)->name) - 1);
    }
    lcm_worker_init(*worker, init_fn);

    res = 0;

exit:
    return res;
}

void lcm_worker_delete(lcm_worker_t** worker, bool forced) {
    ASSERT_NOT_NULL(worker, goto exit);
    ASSERT_NOT_NULL(*worker, goto exit);

    if((*worker)->pid) {
        lcm_worker_stop(*worker, forced);
    }
    lcm_worker_clean(*worker);
    free(*worker);
    *worker = NULL;

exit:
    return;
}

int lcm_worker_init(lcm_worker_t* worker, lcm_worker_init_fn_t init_fn) {
    int res = -1;
    ASSERT_NOT_NULL(worker, goto exit, "Worker missing");
    worker->init_fn = init_fn;
    worker->stop_request = false;
    worker->pid = 0;

    ASSERT_SUCCESS(amxc_llist_new(&worker->tasks), goto exit, "Init Task list failed");

    res = 0;
exit:
    return res;
}

void lcm_worker_clean(lcm_worker_t* worker) {
    ASSERT_NOT_NULL(worker, goto exit);

    amxo_connection_remove(worker->parser, worker->main_fd);
    close(worker->main_fd);
    worker->main_fd = 0;
    worker->worker_fd = 0;
    worker->pid = 0;
    amxc_llist_delete(&worker->tasks, lcm_worker_task_it_free);
    worker->tasks = NULL;

exit:
    return;
}

static int worker_restart(lcm_worker_t* worker) {
    int res = -1;

    // clean up and restart worker with the same config as before
    // TODO: Save the task on the main process to allow trigerring the
    // callback function (if any), and unblock the main process.
    lcm_worker_clean(worker);
    ASSERT_SUCCESS(lcm_worker_init(worker, worker->init_fn), goto exit);
    ASSERT_SUCCESS(lcm_worker_start(worker, worker->parser), goto exit);

    res = 0;
exit:
    return res;
}

static int worker_handle_request(lcm_worker_t* worker) {
    int res = -1;
    lcm_worker_task_t* task = NULL;
    int read_fd = 0;
    lcm_worker_sync_type_t sync_type;

    ASSERT_NOT_NULL(worker, goto exit, "Worker missing. Cannot handle request");

    read_fd = worker->worker_fd;
    ASSERT_NOT_EQUAL(read_fd, 0, goto exit);
    checked_read(read_fd, sync_type, goto exit);

    if(sync_type == lcm_worker_sync_add_task_front) {
        ASSERT_SUCCESS(lcm_worker_task_new(&task), goto exit);
        ASSERT_SUCCESS(worker_task_deserialize(read_fd, task), goto exit);
        ASSERT_SUCCESS(amxc_llist_prepend(worker->tasks, &task->it), goto exit);
    } else if(sync_type == lcm_worker_sync_add_task) {
        ASSERT_SUCCESS(lcm_worker_task_new(&task), goto exit);
        ASSERT_SUCCESS(worker_task_deserialize(read_fd, task), goto exit);
        ASSERT_SUCCESS(amxc_llist_append(worker->tasks, &task->it), goto exit);
    } else if(sync_type == lcm_worker_sync_stop_worker) {
        worker->stop_request = true;
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown type of command received by worker %s", worker->name);
    }

    res = 0;
exit:
    return res;
}

static int worker_trigger_callback(lcm_worker_t* worker, lcm_worker_task_t* task) {
    int res = -1;

    ASSERT_NOT_NULL(worker, goto exit, "Worker missing, cannot send request");
    // signal the main process
    ASSERT_SUCCESS(worker_task_serialize(worker->worker_fd, task), goto exit);

    res = 0;
exit:
    return res;
}

static int worker_execute_task(lcm_worker_t* worker) {
    amxc_llist_it_t* t_it = amxc_llist_take_first(worker->tasks);
    if(!t_it) { // task not found
        return 0;
    }

    lcm_worker_task_t* task = amxc_llist_it_get_data(t_it, lcm_worker_task_t, it);
    task->task_rc = task->task_fn(worker, task->task_var, task->task_rc, task->data);
    if(task->callback_fn) {
        // the callback should be called on the main process
        worker_trigger_callback(worker, task);
    }

    lcm_worker_task_delete(&task);

    return 0;
}

static int worker_loop(lcm_worker_t* worker) {
    int fd = 0;
    fd_set read_fds;
    struct timeval timeout = {.tv_sec = 5, .tv_usec = 0};

    ASSERT_NOT_NULL(worker, return -1, "Worker not missing");
    fd = worker->worker_fd;

    while(1) {
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;

        FD_ZERO(&read_fds);
        FD_SET(fd, &read_fds);

        int ret = select(fd + 1, &read_fds, NULL, NULL, &timeout);
        ASSERT_NOT_EQUAL(ret, -1, break, "Failed in select call (%d: %s)", errno, strerror(errno));
        CHECK_EQUAL_LOG(NOTICE, getppid(), 1, return 1, "Worker parent process died.");
        if(ret == 0) { //timeout
            continue;
        }

        if(ret && FD_ISSET(fd, &read_fds)) {
            ASSERT_SUCCESS(worker_handle_request(worker), continue, "Failed handling request");
        }

        if(worker->stop_request == true) {
            SAH_TRACEZ_INFO(ME, "Stopping worker [%s]", worker->name);
            break;
        }
        worker_execute_task(worker);
    }

    return 0;
}

static int process_cb_fn(int fd, lcm_worker_t* worker) {
    int res = -1;
    lcm_worker_task_t* task = NULL;
    ASSERT_SUCCESS(lcm_worker_task_new(&task), goto exit);
    worker_task_deserialize(fd, task);

    if(task->callback_fn) {
        task->callback_fn(worker, task->callback_var, task->task_rc, task->data);
    } else {
        SAH_TRACEZ_ERROR(ME, "Callback called without callback function");
    }
    lcm_worker_task_delete(&task);
    res = 0;
exit:
    return res;
}

static bool worker_running(pid_t pid) {
    int status;
    pid_t result = waitpid(pid, &status, WNOHANG);
    return result == 0 ? true : false;
}

void process_callback_handler(int fd, void* priv) {
    lcm_worker_t* worker = (lcm_worker_t*) priv;
    fd_set readfds;
    struct timeval timeout = {.tv_sec = 0, .tv_usec = 0};
    pid_t pid = worker->pid;

    ASSERT_NOT_NULL(worker, goto exit);

    if(worker_running(pid) == false) {
        SAH_TRACEZ_ERROR(ME, "Worker [%s] is not running. Restarting it", worker->name);
        ASSERT_SUCCESS(worker_restart(worker), return , "Failed to restart worker [%s]", worker->name)
        return;
    }

    while(1) { // loop and trigger all available callback
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);

        int ret = select(fd + 1, &readfds, NULL, NULL, &timeout);
        ASSERT_NOT_EQUAL(ret, -1, break, "Failed in select (%d: %s)", errno, strerror(errno));
        if(FD_ISSET(fd, &readfds) == 0) {
            break; //No more data available
        }
        process_cb_fn(fd, worker);
        if(worker_running(pid) == false) {
            // stop processing if the worker is stopped by in the cb_fn
            break;
        }
    }

exit:
    return;
}

int lcm_worker_start(lcm_worker_t* worker, amxo_parser_t* parser) {
    int res = -1;
    pid_t pid = 0;
    int fds[2];

    ASSERT_NOT_NULL(worker, goto exit, "Worker missing");
    ASSERT_FALSE(worker->pid, goto exit, "Worker is already running");

    SAH_TRACEZ_INFO(ME, "Start worker [%s]", worker->name);

    // Create communication socket
    ASSERT_SUCCESS(socketpair(PF_LOCAL, SOCK_STREAM | SOCK_CLOEXEC, 0, fds),
                   goto exit, "Could not create signal socketpair (%d: %s)", errno, strerror(errno));

    worker->main_fd = fds[0];
    worker->worker_fd = fds[1];

    pid = fork();
    ASSERT_TRUE(pid != -1, goto exit, "Could not start worker");
    if(pid == 0) {
        // child
        signal(SIGINT, SIG_IGN);
        signal(SIGTERM, SIG_IGN);
        signal(SIGABRT, SIG_IGN);

        close(worker->main_fd);
        worker->stop_request = false;
        if(worker->init_fn) {
            worker->init_fn();
        }

        worker_loop(worker);
        SAH_TRACEZ_NOTICE(ME, "Worker terminated [%s]", worker->name);
        _exit(EXIT_SUCCESS);
    }

    // parent
    close(worker->worker_fd);
    worker->pid = pid;
    if(parser) {
        worker->parser = parser;
        amxo_connection_add(parser, worker->main_fd, process_callback_handler, NULL, AMXO_LISTEN, worker);
    }
    res = 0;
exit:
    return res;
}

int lcm_worker_stop(lcm_worker_t* worker, bool forced) {
    int res = -1;
    int status = 0;
    lcm_worker_sync_type_t sync_type = lcm_worker_sync_stop_worker;

    ASSERT_NOT_NULL(worker, goto exit, "Worker missing, cannot send request");
    ASSERT_NOT_EQUAL(worker->pid, 0, goto exit, "Worker is not running");

    SAH_TRACEZ_INFO(ME, "Stop worker [%s]", worker->name);

    if(forced == false) {
        // signal request to main process
        checked_write(worker->main_fd, sync_type, goto exit);
    } else {
        kill(worker->pid, SIGKILL);
    }

    waitpid(worker->pid, &status, 0);
    worker->pid = 0;
    res = 0;
exit:
    return res;
}

int lcm_worker_add_task(lcm_worker_t* worker, lcm_worker_task_t* task, bool add_to_front) {
    int res = -1;
    lcm_worker_sync_type_t sync_type;

    ASSERT_NOT_NULL(worker, goto exit, "Worker missing, cannot send request");

    sync_type = add_to_front ? lcm_worker_sync_add_task_front : lcm_worker_sync_add_task;

    // signal the main process
    checked_write(worker->main_fd, sync_type, goto exit);
    ASSERT_SUCCESS(worker_task_serialize(worker->main_fd, task), goto exit, "Failed to send task to worker process");
    res = 0;
exit:
    lcm_worker_task_delete(&task);
    return res;
}
