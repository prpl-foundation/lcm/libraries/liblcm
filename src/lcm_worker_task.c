/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>

#include "lcm/lcm_assert.h"
#include "lcm_priv.h"
#include "lcm/lcm_worker_task.h"
#include "lcm/lcm_worker.h"
#include "lcm/lcm_serialize.h"
#include <debug/sahtrace.h>

#define ME "worker_task"


int lcm_worker_task_new(lcm_worker_task_t** task) {
    int retval = -1;
    ASSERT_NOT_NULL(task, goto exit);

    *task = (lcm_worker_task_t*) calloc(1, sizeof(lcm_worker_task_t));
    ASSERT_NOT_NULL(*task, goto exit);

    ASSERT_SUCCESS(amxc_llist_it_init(&(*task)->it), goto error, "Failed to init task");
    retval = 0;
    goto exit;

error:
    lcm_worker_task_delete(task);
exit:
    return retval;
}

void lcm_worker_task_delete(lcm_worker_task_t** task) {
    ASSERT_NOT_NULL(task, goto exit);
    ASSERT_NOT_NULL(*task, goto exit);

    if((*task)->task_var) {
        amxc_var_delete(&((*task)->task_var));
    }
    if((*task)->data) {
        amxc_var_delete(&((*task)->data));
    }

    free(*task);
    *task = NULL;
exit:
    return;
}

void lcm_worker_task_it_free(amxc_llist_it_t* t_it) {
    lcm_worker_task_t* task = amxc_llist_it_get_data(t_it, lcm_worker_task_t, it);
    lcm_worker_task_delete(&task);
}

int lcm_worker_task_add_function(lcm_worker_task_t* task,
                                 lcm_worker_task_fn_t fn,
                                 amxc_var_t* var) {
    int res = -1;

    ASSERT_NOT_NULL(task, goto exit);
    ASSERT_NOT_NULL(fn, goto exit);
    task->task_fn = fn;
    if(var) {
        amxc_var_new(&(task->task_var));
        amxc_var_copy(task->task_var, var);
    }

    res = 0;
exit:
    return res;

}

int lcm_worker_task_add_callback(lcm_worker_task_t* task,
                                 lcm_worker_task_fn_t fn,
                                 amxc_var_t* var) {
    int res = -1;

    ASSERT_NOT_NULL(task, goto exit);
    ASSERT_NOT_NULL(fn, goto exit);
    task->callback_fn = fn;
    task->callback_var = var;

    res = 0;
exit:
    return res;
}

int lcm_worker_task_add_data(lcm_worker_task_t* task, amxc_var_t* data) {
    int res = -1;

    ASSERT_NOT_NULL(task, goto exit);
    ASSERT_NOT_NULL(data, goto exit);

    if(task->data) {
        amxc_var_delete(&task->data);
    }

    amxc_var_new(&(task->data));
    amxc_var_copy(task->data, data);

    res = 0;
exit:
    return res;
}

int worker_task_serialize(int fd, const lcm_worker_task_t* worker_task) {
    int res = -1;

    checked_write(fd, worker_task->task_fn, goto exit);
    bool has_var = worker_task->task_var != NULL;
    checked_write(fd, has_var, goto exit);
    if(has_var) {
        lcm_var_serialize(fd, worker_task->task_var);
    }
    checked_write(fd, worker_task->task_rc, goto exit);
    checked_write(fd, worker_task->callback_fn, goto exit);
    // this var should not be serialized, since the callback should
    // happen on the main process where this pointer should resolve
    // to the correct data
    checked_write(fd, worker_task->callback_var, goto exit);
    bool has_data = worker_task->data != NULL;
    checked_write(fd, has_data, goto exit);
    if(has_data) {
        lcm_var_serialize(fd, worker_task->data);
    }
    res = 0;

exit:
    return res;
}


int worker_task_deserialize(int fd, lcm_worker_task_t* worker_task) {
    int res = -1;
    bool has_var = false;
    bool has_data = false;

    checked_read(fd, worker_task->task_fn, goto exit);
    checked_read(fd, has_var, goto exit);
    if(has_var) {
        amxc_var_new(&(worker_task->task_var));
        lcm_var_deserialize(fd, worker_task->task_var);
    }
    checked_read(fd, worker_task->task_rc, goto exit);
    checked_read(fd, worker_task->callback_fn, goto exit);
    checked_read(fd, worker_task->callback_var, goto exit);
    checked_read(fd, has_data, goto exit);
    if(has_data) {
        amxc_var_new(&(worker_task->data));
        lcm_var_deserialize(fd, worker_task->data);
    }
    res = 0;
exit:
    return res;
}
