/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <fcntl.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>

#include <lcm/lcm_worker.h>
#include <lcm/lcm_worker_task.h>
#include <lcm/lcm_assert.h>
#include "lcm_priv.h"
#include "test_lcm_worker.h"
#include "log.h"

#define ME "test"

typedef enum _check {
    check_init_called = 0,
    check_task_called,
    check_task_cb_called,
    check_task_with_params_cb_called,
    check_cb_called,
    check_test_task_1_called,
    check_test_task_1_cb_called,
    check_test_task_2_called,
    check_thread_check,
    check_thread_cb_check,
    check_last
} check_t;

static const char* check_names[] = {
    "init_called",
    "task_called",
    "task_cb_called",
    "check_task_with_params_cb_called",
    "cb_called",
    "test_task_1_called",
    "test_task_1_cb_called",
    "test_task_2_called",
    "thread_check",
    "thread_cb_check"
};

static const char* check_to_string(check_t check) {
    if((check >= 0) && (check < check_last)) {
        return check_names[check];
    } else {
        return NULL;
    }
}

static char* signaldir = NULL;


static pid_t main_pid = 0;

#define DO_CALLBACK "callback"

static bool is_on_mainprocess(void) {
    return getpid() == main_pid;
}

static void set_check(check_t check) {
    char filename[1024];
    sprintf(filename, "%s/%s", signaldir, check_to_string(check));
    int fd = creat(filename, S_IRWXU);
    close(fd);
}

static bool get_check(check_t check) {
    char filename[1024];
    struct stat fileStat;
    sprintf(filename, "%s/%s", signaldir, check_to_string(check));

    if(lstat(filename, &fileStat) < 0) {
        return false;
    }
    return true;
}

static void reset_check(check_t check) {
    char filename[1024];
    struct stat fileStat;
    sprintf(filename, "%s/%s", signaldir, check_to_string(check));
    if(lstat(filename, &fileStat) == 0) {
        unlink(filename);
    }
}

static void init_globals(void) {
    reset_check(check_init_called);
    reset_check(check_task_called);
    reset_check(check_task_cb_called);
    reset_check(check_task_with_params_cb_called);
    reset_check(check_cb_called);
    reset_check(check_thread_check);
    reset_check(check_thread_cb_check);
    reset_check(check_test_task_1_called);
    reset_check(check_test_task_1_cb_called);
    reset_check(check_test_task_2_called);
}
/*************/
/* Callbacks */
/*************/

static int test_task_callback(UNUSED lcm_worker_t* worker, UNUSED amxc_var_t* var, int rc, UNUSED amxc_var_t* data) {
    assert_true(is_on_mainprocess());
    assert_int_equal(rc, 5);
    set_check(check_task_cb_called);
    return 0;
}

static int test_task_callback_with_params(UNUSED lcm_worker_t* worker, amxc_var_t* var, int rc, UNUSED amxc_var_t* data) {
    assert_true(is_on_mainprocess());
    assert_non_null(var);
    uint32_t val = GET_INT32(var, "intvalue");
    assert_int_equal(val, 22);
    assert_int_equal(rc, 5);
    set_check(check_task_with_params_cb_called);
    amxc_var_delete(&var);
    return 0;
}

static int test_task_1_callback(UNUSED lcm_worker_t* worker, UNUSED amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    assert_true(is_on_mainprocess());
    sleep(1);
    //lcm_worker_task_completed(worker);
    set_check(check_test_task_1_cb_called);
    return 0;
}

/***********/
/* Workers */
/***********/
static void lcm_worker_init_function(void) {
    set_check(check_init_called);
    return;
}

static int test_task(UNUSED lcm_worker_t* worker, amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    bool call_callback = GET_BOOL(var, DO_CALLBACK);
    SAH_TRACEZ_INFO(ME, "Callback data: %d", call_callback);
    if(is_on_mainprocess()) {
        reset_check(check_thread_check);
    } else {
        set_check(check_thread_check);
    }
    set_check(check_task_called);

    return 5;
}

static int test_task_1(UNUSED lcm_worker_t* worker, UNUSED amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    set_check(check_test_task_1_called);
    return 5;
}

static int test_task_2(UNUSED lcm_worker_t* worker, UNUSED amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    set_check(check_test_task_2_called);
    return 5;
}

static void handle_events(lcm_worker_t* worker) {
    while(amxp_signal_read() == 0) {
    }
    process_callback_handler(worker->main_fd, worker);
}

int test_lcm_worker_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDOUT);
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "all");

    main_pid = getpid();
    char template[] = "/tmp/lcmworker.XXXXXX";
    signaldir = strdup(mkdtemp(template));
    return 0;
}

int test_lcm_worker_teardown(UNUSED void** state) {
    free(signaldir);
    return 0;
}

void test_lcm_worker_new_delete(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    init_globals();
    assert_int_equal(lcm_worker_new(&worker, NULL, NULL), 0);
    assert_non_null(worker);
    assert_non_null(worker->tasks);
    assert_null(worker->init_fn);
    assert_int_equal(worker->pid, 0);

    lcm_worker_delete(&worker, false);
    assert_null(worker);

    // with init function
    assert_int_equal(lcm_worker_new(&worker, lcm_worker_init_function, NULL), 0);
    assert_non_null(worker);
    assert_non_null(worker->tasks);
    assert_non_null(worker->init_fn);
    assert_int_equal(worker->pid, 0);

    lcm_worker_delete(&worker, false);
    assert_null(worker);
    LOG_EXIT
}

void test_lcm_worker_start_stop(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    init_globals();
    assert_int_equal(lcm_worker_new(&worker, NULL, "name"), 0);
    assert_non_null(worker);

    assert_int_equal(lcm_worker_start(worker, NULL), 0);
    sleep(1);
    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    assert_null(worker);

    // with init function
    reset_check(check_init_called);
    assert_int_equal(lcm_worker_new(&worker, lcm_worker_init_function, "reallyreallyreallylongname"), 0);
    assert_int_equal(lcm_worker_start(worker, NULL), 0);
    sleep(1);
    assert_true(get_check(check_init_called));
    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    assert_null(worker);
    LOG_EXIT
}

void test_lcm_worker_task_new_delete(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_task_t* worker_task = NULL;
    init_globals();
    assert_int_equal(lcm_worker_task_new(&worker_task), 0);
    assert_non_null(worker_task);
    assert_null(worker_task->task_var);

    lcm_worker_task_delete(&worker_task);
    assert_null(worker_task);
    LOG_EXIT
}

void test_lcm_worker_add_task(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    lcm_worker_task_t* task = NULL;
    init_globals();
    assert_int_equal(lcm_worker_new(&worker, NULL, NULL), 0);
    assert_int_equal(lcm_worker_task_new(&task), 0);
    assert_int_equal(lcm_worker_task_add_function(task, test_task, NULL), 0);
    assert_non_null(worker);
    assert_true(is_on_mainprocess());


    assert_int_equal(lcm_worker_start(worker, NULL), 0);
    lcm_worker_add_task(worker, task, false);
    handle_events(worker);
    for(int i = 0; i < 3; i++) {
        if(get_check(check_task_called)) {
            break;
        }
        sleep(1);
        handle_events(worker);
    }
    assert_true(get_check(check_task_called));

    assert_true(get_check(check_thread_check));

    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    assert_null(worker);
    LOG_EXIT
}

void test_lcm_worker_add_task_with_cb(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    lcm_worker_task_t* task = NULL;
    amxo_parser_t* parser = NULL;
    init_globals();

    assert_int_equal(amxo_parser_new(&parser), 0);
    assert_int_equal(lcm_worker_new(&worker, NULL, NULL), 0);
    assert_non_null(worker);
    assert_int_equal(lcm_worker_task_new(&task), 0);
    assert_int_equal(lcm_worker_task_add_function(task, test_task, NULL), 0);
    assert_int_equal(lcm_worker_task_add_callback(task, test_task_callback, NULL), 0);

    assert_true(is_on_mainprocess());

    assert_int_equal(lcm_worker_start(worker, parser), 0);
    lcm_worker_add_task(worker, task, false);
    handle_events(worker);
    for(int i = 0; i < 10; i++) {
        if(get_check(check_task_called) && get_check(check_task_cb_called)) {
            break;
        }
        sleep(1);
        handle_events(worker);
    }

    assert_true(get_check(check_task_called));
    assert_true(get_check(check_task_cb_called));
    assert_true(get_check(check_thread_check));

    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    amxo_parser_delete(&parser);
    assert_null(worker);
    LOG_EXIT
}

void test_lcm_worker_add_task_with_cb_params(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    lcm_worker_task_t* task = NULL;
    amxo_parser_t* parser = NULL;
    amxc_var_t* cb_params = NULL;
    init_globals();

    assert_int_equal(amxo_parser_new(&parser), 0);
    assert_int_equal(lcm_worker_new(&worker, NULL, NULL), 0);
    assert_non_null(worker);
    assert_true(is_on_mainprocess());
    assert_int_equal(lcm_worker_start(worker, parser), 0);

    assert_int_equal(lcm_worker_task_new(&task), 0);
    assert_int_equal(lcm_worker_task_add_function(task, test_task, NULL), 0);
    assert_int_equal(amxc_var_new(&cb_params), 0);
    assert_int_equal(amxc_var_set_type(cb_params, AMXC_VAR_ID_HTABLE), 0);
    assert_non_null(amxc_var_add_new_key_int32_t(cb_params, "intvalue", 22));
    assert_int_equal(lcm_worker_task_add_callback(task, test_task_callback_with_params, cb_params), 0);
    lcm_worker_add_task(worker, task, false);
    handle_events(worker);
    for(int i = 0; i < 10; i++) {
        if(get_check(check_task_called) && get_check(check_task_with_params_cb_called)) {
            break;
        }
        sleep(1);
        handle_events(worker);
    }

    assert_true(get_check(check_task_called));
    assert_true(get_check(check_task_with_params_cb_called));
    assert_true(get_check(check_thread_check));

    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    amxo_parser_delete(&parser);
    assert_null(worker);
    LOG_EXIT
}

void test_lcm_worker_test_task_complete(UNUSED void** state) {
    LOG_ENTRY
    lcm_worker_t* worker = NULL;
    lcm_worker_task_t* task1 = NULL;
    lcm_worker_task_t* task2 = NULL;
    amxo_parser_t* parser = NULL;
    init_globals();

    assert_int_equal(amxo_parser_new(&parser), 0);
    assert_int_equal(lcm_worker_new(&worker, NULL, NULL), 0);
    assert_non_null(worker);

    assert_int_equal(lcm_worker_task_new(&task1), 0);
    assert_int_equal(lcm_worker_task_add_function(task1, test_task_1, NULL), 0);
    assert_int_equal(lcm_worker_task_add_callback(task1, test_task_1_callback, NULL), 0);

    assert_int_equal(lcm_worker_task_new(&task2), 0);
    assert_int_equal(lcm_worker_task_add_function(task2, test_task_2, NULL), 0);


    assert_true(is_on_mainprocess());

    assert_int_equal(lcm_worker_start(worker, parser), 0);
    lcm_worker_add_task(worker, task1, false);
    lcm_worker_add_task(worker, task2, false);
    handle_events(worker);
    for(int i = 0; i < 5; i++) {
        if(get_check(check_test_task_1_called) && get_check(check_test_task_1_cb_called) && get_check(check_test_task_2_called)) {
            break;
        }
        sleep(1);
        handle_events(worker);
    }

    assert_true(get_check(check_test_task_1_called));
    assert_true(get_check(check_test_task_1_cb_called));
    assert_true(get_check(check_test_task_2_called));

    lcm_worker_stop(worker, false);

    lcm_worker_delete(&worker, false);
    amxo_parser_delete(&parser);
    assert_null(worker);
    LOG_EXIT
}

